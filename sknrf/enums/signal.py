from collections import OrderedDict
from enum import Enum

from sknrf.device.signal import tf
from sknrf.view.widget.qtpropertybrowser import Config

transform_map = OrderedDict([
    (Config.Domain.TF, tf.tf),
    (Config.Domain.FF, tf.ff),
    (Config.Domain.FT, tf.ft),
    (Config.Domain.TT, tf.tt),
])

transform_label_map = OrderedDict([
    (Config.Domain.TF, "Envelope"),
    (Config.Domain.FF, "Frequency"),
    (Config.Domain.FT, "Switching"),
    (Config.Domain.TT, "Transient"),
])

transform_icon_map = {
    Config.Domain.TF: ":/SVG/green/circled_fill.svg",
    Config.Domain.FF: ":/SVG/red/circled_fill.svg",
    Config.Domain.FT: ":/SVG/blue/circled_fill.svg",
    Config.Domain.TT: ":/SVG/cyan/circled_fill.svg",
}

transform_color_map = {
    Config.Domain.TF: "green",
    Config.Domain.FF: "red",
    Config.Domain.FT: "blue",
    Config.Domain.TT: "cyan",
}

transform_xlabel_map = {
    Config.Domain.TF: r"$time$ $[s]$",
    Config.Domain.FF: r"$freq$ $[Hz]$",
    Config.Domain.FT: r"$time$ $[s]$",
    Config.Domain.TT: r"$time$ $[s]$",
}
