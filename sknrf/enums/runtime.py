from enum import Enum, Flag, auto, unique

import numpy as np
import torch as th

from sknrf.view.widget.qtpropertybrowser import Config


@unique
class RuntimeState(Enum):
    RUN = 0
    SINGLE = 1
    PAUSED = 2
    STOPPED = 3


class Bound(Flag):
    OFF = 0  # No checks
    OUT = auto()  # Functionals optionally define output bound checking
    IN = auto()  # Functionals optionally define input bound checking
    SET = auto()  # Tensor defines bound checking during Tensor.__setitem__
    GET = auto()  # Tensor defines bound checking during Tensor.__getitem__
    LEVEL1 = OUT
    LEVEL2 = LEVEL1 | IN
    LEVEL3 = LEVEL2 | SET
    LEVEL4 = LEVEL3 | GET


th_2_np_dtype_map = {
    th.int: int,
    th.int8: np.int8,
    th.int16: np.int16,
    th.int32: np.int32,
    th.int64: np.int64,
    th.float: float,
    th.float16: np.float16,
    th.float32: np.float32,
    th.float64: np.float64,
    th.complex: complex,
    # th.complex32: np.complex32,
    th.complex64: np.complex64,
    th.complex128: np.complex128,
}


class SI (Enum):
    F = auto()
    T = auto()
    V = auto()
    I = auto()
    Z = auto()
    B = auto()
    A = auto()
    G = auto()
    TEMP = auto()


si_dtype_map = {
    SI.F: th.double,
    SI.T: th.double,
    SI.V: th.complex128,
    SI.I: th.complex128,
    SI.Z: th.complex128,
    SI.B: th.complex128,
    SI.A: th.complex128,
    SI.G: th.complex128,
    SI.TEMP: th.float,
}


si_eps_map = {
    SI.F:    1,
    SI.T:    th.finfo(si_dtype_map[SI.T]).eps*2**3,
    SI.V:    np.conj(50.0)/np.sqrt(np.real(50.0))*th.finfo(si_dtype_map[SI.A]).eps*2**3,  # a2v()
    SI.I:    th.finfo(si_dtype_map[SI.I]).eps*2**3,
    SI.Z:    th.finfo(si_dtype_map[SI.Z]).eps*2**3,
    SI.B:    th.finfo(si_dtype_map[SI.I]).eps*2**3,
    SI.A:    th.finfo(si_dtype_map[SI.A]).eps*2**3,
    SI.G:    th.finfo(si_dtype_map[SI.G]).eps*2**3,
    SI.TEMP: th.finfo(si_dtype_map[SI.TEMP]).eps*2**3,
}


si_format_map = {
    SI.F:    Config.Format.RE,
    SI.T:    Config.Format.RE,
    SI.V:    Config.Format.RE_IM,
    SI.I:    Config.Format.RE_IM,
    SI.Z:    Config.Format.RE_IM,
    SI.B:    Config.Format.LOG_DEG,
    SI.A:    Config.Format.LOG_DEG,
    SI.G:    Config.Format.LIN_DEG,
    SI.TEMP: Config.Format.RE,
}


si_scale_map = {
    SI.F:    Config.Scale.G,
    SI.T:    Config.Scale.m,
    SI.V:    Config.Scale._,
    SI.I:    Config.Scale._,
    SI.Z:    Config.Scale._,
    SI.B:    Config.Scale.m,
    SI.A:    Config.Scale.m,
    SI.G:    Config.Scale._,
    SI.TEMP: Config.Scale._,
}


si_unit_map = {
    SI.F:    "Hz",
    SI.T:    "s",
    SI.V:    "V",
    SI.I:    "A",
    SI.Z:    "ohm",
    SI.B:    "rW",
    SI.A:    "rW",
    SI.G:    "U",
    SI.TEMP: "K",
}



