from dataclasses import dataclass

from sknrf.device.simulator.qucs.components.base import AbstractComponent


@dataclass
class PortSym(AbstractComponent):
    """ PortSym Symbol

        Parameters:
        _label (str, optional): Schematic label. Defaults to '.PortSym'.

        u0 (int): Unknown Parameter 0.
        u1 (int): Unknown Parameter 0.
        u2 (int): Unknown Parameter 0.
        u3 (int): Unknown Parameter 0.
        Name (str): Port RefID.
    """
    _label = '.PortSym'

    u0: int
    u1: int
    u2: int
    u3: int
    Name: str
