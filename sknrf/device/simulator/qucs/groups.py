import re
from abc import ABC
from dataclasses import dataclass
from typing import List


class AbstractGroup(ABC, dict):
    """ Abstract Group

        Parameters:
        _label (str, optional): Schematic label. Defaults to ''.
        _pattern (str, optional): Schematic regex. Defaults to '(?:[^\\s"]+|\"[^\"]*\")'.
        _title_sep (str, optional): Character that separates title from attributes. Defaults to ''.
        _attr_sep (str, optional): Character that separates attributes. Defaults to '\n'.
        _hidden (bool, optional): If True, don't show tag in .sch file. Defaults to 'False'.
    """
    _label = ''
    _pattern = r'(?:[^\s"]+|\"[^\"]*\")'
    _title_sep = ''
    _attr_sep = '\n'
    _hidden = False  # Don't show tag in .sch file

    @classmethod
    def parse(cls, text) -> List:
        """  Parse the Unformatted XML-like tag
            - [ ^\\s"]+: Matches a sequence of characters that are neither spaces nor double quotes. This part will match "normal" words.
            - \"[^\"]*\": Matches any sequence of characters enclosed by double quotes. [^"]* ensures that it matches any characters except for double quotes inside the quotes.

        Parameters
        ----------
        text

        Returns
        -------

        """
        text = text.lstrip('<').rstrip('>')
        attr_start = text.index(cls._title_sep) + len(cls._title_sep)
        attrs = text[attr_start::]
        args = re.findall(cls._pattern, attrs)
        return args

    def xml_tag(self, indent=0):
        indent_str = ' ' * 2 * indent
        tag = f'{indent_str:s}<{self._label:s}>\n'
        return tag

    def sch_tag(self, indent=0):
        indent_str = ' ' * 2 * indent
        tag = f'' if self._hidden else f'{indent_str:s}<{self._label:s}>\n'
        return tag


@dataclass
class Schematic(AbstractGroup):
    """ Schematic Group

        Parameters:
        name (str, optional): Name of the Group. Defaults to 'Schematic'.

        Returns:
        Schematic: Schematic Group
    """
    _label = 'Schematic'
    _hidden = True

    Name: str = 'Schematic'


@dataclass
class Properties(AbstractGroup):
    """ Properties Group

        Parameters:
        name (str, optional): Name of the Group. Defaults to 'Properties'.

        Returns:
        Properties: Properties Group
    """
    _label = 'Properties'

    Name: str = 'Properties'


@dataclass
class Symbol(AbstractGroup):
    """ Symbol Group

        Parameters:
        name (str, optional): Name of the Group. Defaults to 'Symbol'.

        Returns:
        Symbol: Symbol Group
    """
    _label = 'Symbol'

    Name: str


@dataclass
class Components(AbstractGroup):
    """ Components Group

        Parameters:
        name (str, optional): Name of the Group. Defaults to 'Components'.

        Returns:
        Components: Components Group
    """
    _label = 'Components'

    Name: str


@dataclass
class Wires(AbstractGroup):
    """ Wires Group

        Parameters:
        name (str, optional): Name of the Group. Defaults to 'Wires'.

        Returns:
        Wires: Wires Group
    """
    _label = 'Wires'

    Name: str


@dataclass
class Diagrams(AbstractGroup):
    """ Diagrams Group

        Currently assuming that group is empty

        Parameters:
        name (str, optional): Name of the Group. Defaults to 'Diagrams'.

        Returns:
        Diagrams: Diagrams Group
    """
    _label = 'Diagrams'

    Name: str


@dataclass
class Paintings(AbstractGroup):
    """ Paintings Group

        Currently assuming that group is empty

        Parameters:
        name (str, optional): Name of the Group. Defaults to 'Paintings'.

        Returns:
        Paintings: Paintings Group
    """
    _label = 'Paintings'

    Name: str
