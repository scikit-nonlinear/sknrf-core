import xml.etree.ElementTree as ET

from sknrf.device.simulator.qucs.groups import *
from sknrf.device.simulator.qucs.properties import *
from sknrf.device.simulator.qucs.symbol import *
from sknrf.device.simulator.qucs.components.equations import *
from sknrf.device.simulator.qucs.components.file import *
from sknrf.device.simulator.qucs.components.line import *
from sknrf.device.simulator.qucs.components.lumped import *
from sknrf.device.simulator.qucs.components.port import *
from sknrf.device.simulator.qucs.components.probes import *
from sknrf.device.simulator.qucs.components.rf import *
from sknrf.device.simulator.qucs.components.simulations import *
from sknrf.device.simulator.qucs.components.sources import *
from sknrf.device.simulator.qucs.wires import *
from sknrf.device.simulator.qucs.diagrams import *
from sknrf.device.simulator.qucs.paintings import *

# from sknrf.device.simulator.base import AbstractSimulator, SimulatorError
# from sknrf.device.simulator.qucs.parse_result import QucsDataset
# from sknrf.settings import Settings
#
# __author__ = 'dtbespal'
# logger = logging.getLogger(__name__)


def import_schematic(in_filename, sch_filename, xml_filename):
    tree = {}
    tag = []
    sch_indent = 0
    xml_indent = 0
    leaf_tag = ''
    groups = ['Properties', 'Symbol', 'Components', 'Wires', 'Diagrams', 'Paintings']
    with open(in_filename, 'rt') as fin:
        with open(sch_filename, 'wt') as fsch:
            with open(xml_filename, 'wt') as fxml:
                line = f'<Schematic>'
                leaf_tag = line.lstrip('<').rstrip('>')
                tag.append(leaf_tag)
                obj = eval(f'{tag[-1]:s}(*{tag[-1]:s}.parse(line))')
                sch_line = eval(f'obj.sch_tag(indent=sch_indent)')
                xml_line = eval(f'obj.xml_tag(indent=xml_indent)')
                sch_indent += 0
                xml_indent += 1
                fsch.write(f'{sch_line:s}')
                fxml.write(f'{xml_line:s}')
                while True:
                    line = fin.readline()
                    if len(line) == 0:  # EOF
                        break
                    line = line.strip()
                    if len(line) == 0:  # Empty Line
                        pass
                    elif len(leaf_tag:=line.lstrip('<').lstrip('/').rstrip('>')) == len(line) - 3:  # Group End
                        if len(tag) == 2:
                            if leaf_tag in groups:
                                sch_indent -= 1
                                xml_indent -= 1
                                obj = eval(f'{tag[-1]:s}(*{tag[-1]:s}.parse(line))')
                                sch_line = eval(f'obj.sch_tag(indent=sch_indent)')
                                xml_line = eval(f'obj.xml_tag(indent=xml_indent)')
                                sch_line = sch_line.replace('<', '</')
                                xml_line = xml_line.replace('<', '</')
                            else:
                                raise AttributeError(f'Unsupported Group Tag: </{leaf_tag:s}>')
                            fsch.write(f'{sch_line:s}')
                            fxml.write(f'{xml_line:s}')
                            del tag[-1]
                        else:
                            raise ValueError(f'Unsupported Nested tag, len(tag): {len(tag):d}')
                    elif len(leaf_tag:=line.lstrip('<').rstrip('>')) == len(line) - 2:
                        leaf_tag = leaf_tag.lstrip('.')
                        if len(tag) == 1:  # Group Start
                            if line.startswith('<Qucs Schematic'):
                                continue
                            elif leaf_tag in groups:
                                tag.append(leaf_tag)
                            else:
                                raise AttributeError(f'Unsupported Group Tag: <{leaf_tag:s}>')
                            try:
                                obj = eval(f'{tag[-1]:s}(*{tag[-1]:s}.parse(line))')
                                sch_line = eval(f'obj.sch_tag(indent=sch_indent)')
                                xml_line = eval(f'obj.xml_tag(indent=xml_indent)')
                            except Exception as e:
                                raise AttributeError(f'Unknown tag: <{tag[0]:s}>:<{tag[1]:s}>')
                            sch_indent += 1
                            xml_indent += 1
                            fsch.write(f'{sch_line:s}')
                            fxml.write(f'{xml_line:s}')
                            tree[tag[1]] = obj
                        elif len(tag) == 2:  # Component Start
                            if tag[1] == 'Properties':
                                tag.append(leaf_tag[0:leaf_tag.index('=')])
                            elif tag[1] == 'Symbol':
                                tag.append(leaf_tag[0:leaf_tag.index(' ')])
                            elif tag[1] == 'Components':
                                tag.append(leaf_tag[0:leaf_tag.index(' ')])
                            elif tag[1] == 'Wires':
                                tag.append('Wire')
                            elif tag[1] == 'Diagrams':
                                continue
                            elif tag[1] == 'Paintings':
                                continue
                            try:
                                obj = eval(f'{tag[-1]:s}(*{tag[-1]:s}.parse(line))')
                                sch_line = eval(f'obj.sch_tag(indent=sch_indent)')
                                xml_line = eval(f'obj.xml_tag(indent=xml_indent)')
                            except Exception as e:
                                raise AttributeError(f'Unknown tag: <{tag[0]:s}>:<{tag[1]:s}>:<{tag[2]:s}>')
                            fsch.write(f'{sch_line:s}')
                            fxml.write(f'{xml_line:s}')
                            tree[tag[1]][tag[2]] = obj
                            del tag[-1]
                        else:
                            raise ValueError(f'Unsupported Nested tag, len(tag): {len(tag):d}')
                    else:
                        raise ValueError(f'Unparsable line: {line:s}')
                # End While
                line = f'</Schematic>'
                leaf_tag = line.lstrip('<').lstrip('/').rstrip('>')
                sch_indent -= 0
                xml_indent -= 1
                obj = eval(f'{tag[-1]:s}(*{tag[-1]:s}.parse(line))')
                sch_line = eval(f'obj.sch_tag(indent=sch_indent)')
                xml_line = eval(f'obj.xml_tag(indent=xml_indent)')
                sch_line = sch_line.replace('<', '</')
                xml_line = xml_line.replace('<', '</')
                fsch.write(f'{sch_line:s}')
                fxml.write(f'{xml_line:s}')
                del tag[-1]


# class QucsSimulator(AbstractSimulator):
#     """ Remote Control over Qucs Simulator using read/write of Qucs .sch schematic file and read of .dat dataset file
#         Used to write input conditions to an existing Qucs .sch schematic file, performs the simulation, and then collect
#         simulation results from a .dat dataset file.
#     """
#
#     def __init__(self, netlist_filename, dataset_filename, simulator_type, simulator_name, config_filename="",
#                  remote_host="", remote_user="", remote_password="", remote_key_filename="", remote_port=22):
#
#         assert len(remote_host) == 0, "Remote simulation is not supported"
#         assert len(remote_user) == 0, "Remote simulation is not supported"
#         assert len(remote_password) == 0, "Remote simulation is not supported"
#         assert len(remote_key_filename) == 0, "Remote simulation is not supported"
#
#         super(QucsSimulator, self).__init__(netlist_filename, dataset_filename,
#                                            remote_host=remote_host, remote_user=remote_user,
#                                            remote_password=remote_password, remote_key_filename=remote_key_filename,
#                                            remote_port=remote_port)
#
#         self._tree = ET.parse(self._netlist_filename)
#         self._root = self._tree.getroot()
#
#         self._simulator_type = ""
#         self._simulator_name = ""
#         self._config_filename, self._remote_config_filename = "", ""
#         # self.set_config_filename(config_filename)
#
#         # Set the Simulator ID
#         self.order = 5
#         self.simulator_type = simulator_type
#         self.simulator_name = simulator_name
#
#     def set_config_filename(self, config_filename):
#         self._config_filename, self._remote_config_filename = self._set_filename(config_filename, "rt")
#
#     @property
#     def simulator_type(self):
#         """ ADS Simulator Type get method.
#         :return: simulator_name eg("HB")
#         """
#         return self._simulator_type
#
#     @simulator_type.setter
#     def simulator_type(self, simulator_type):
#         """ ADS Simulator Type set method.
#         :param: simulator_type eg("HB")
#         """
#         self._simulator_type = simulator_type
#
#     @property
#     def simulator_name(self):
#         """ ADS Simulator Name get method.
#         :return: simulator_name eg("HB1")
#         """
#         return self._simulator_name
#
#     @simulator_name.setter
#     def simulator_name(self, simulator_name):
#         """ ADS Simulator Name set method.
#         :param simulator_name: simulator_name eg("HB1")
#         :return:
#         """
#         self._simulator_name = simulator_name
#
#     @property
#     def simulator_id(self):
#         """ ADS Simulator ID get method.
#         :return simulator_id: simulator_name.simulator_type
#         """
#         return self.simulator_name + "." + self.simulator_type
#
#     def preset(self):
#         # todo
#         super(QucsSimulator, self).preset()
#         # filename, ext = os.path.splitext(self._config_filename)
#         # shutil.copyfile(filename[:-7] + ext, filename + ext)
#
#         # Pre-Format Netlist File for easier parsing:
#         # Convert mutli-line blocks to single line for easier parsing
#         with open(self._netlist_filename, "rt") as file:
#             file_str = file.read()
#             file_str = re.sub(r"\s+\\\s*\n", " ", file_str)
#
#         with open(self._netlist_filename, "wt") as file:
#             print(file_str, file=file, sep="", end="", flush=True)
#
#         # Write the Dataset filename to the netlist
#         if self.is_remote():
#             self.write(r'MatlabOutput.Argument\[0\]', self._remote_dataset_filename)
#         else:
#             self.write(r'MatlabOutput.Argument\[0\]', self._dataset_filename)
#
#         try:
#             self.write(self.simulator_name + ".Stop", Settings().t_stop)
#             self.write(self.simulator_name + ".Step", Settings().t_step)
#             self.write("_fund_1", Settings().f0)
#             if int(self.read_netlist(self.simulator_name + ".Order\[1\]").real) < Settings().num_harmonics:
#                 raise ValueError("Number of harmonics: " + str(Settings().num_harmonics) + "exceeds maximum harmonics: 5")
#         except AttributeError:
#             logger.warning("Could not preset simulator controller")
#
#         # Initialize the IQ files to CW values
#         for port_index in range(1, 3):
#             np.savetxt(os.sep.join((Settings().data_root,"simulation", "V%d%d_I.txt" % (port_index, 0))),
#                            np.ones(Settings().t_points, dtype=np.float64).reshape((1, -1)))
#             np.savetxt(os.sep.join((Settings().data_root,"simulation", "V%d%d_Q.txt" % (port_index, 0))),
#                            np.zeros(Settings().t_points, dtype=np.float64).reshape((1, -1)))
#             np.savetxt(os.sep.join((Settings().data_root,"simulation", "Z%d%d_I.txt" % (port_index, 0))),
#                            np.ones(Settings().t_points, dtype=np.float64).reshape((1, -1)))
#             np.savetxt(os.sep.join((Settings().data_root,"simulation", "Z%d%d_Q.txt" % (port_index, 0))),
#                            np.zeros(Settings().t_points, dtype=np.float64).reshape((1, -1)))
#             for harm_idx in range(1, 6):
#                 np.savetxt(os.sep.join((Settings().data_root,"simulation", "A%d%d_I.txt" % (port_index, harm_idx))),
#                            np.ones(Settings().t_points, dtype=np.float64).reshape((1, -1)))
#                 np.savetxt(os.sep.join((Settings().data_root,"simulation", "A%d%d_Q.txt" % (port_index, harm_idx))),
#                            np.zeros(Settings().t_points, dtype=np.float64).reshape((1, -1)))
#                 np.savetxt(os.sep.join((Settings().data_root,"simulation", "Z%d%d_I.txt" % (port_index, harm_idx))),
#                            np.ones(Settings().t_points, dtype=np.float64).reshape((1, -1)))
#                 np.savetxt(os.sep.join((Settings().data_root,"simulation", "Z%d%d_Q.txt" % (port_index, harm_idx))),
#                            np.zeros(Settings().t_points, dtype=np.float64).reshape((1, -1)))
#
#     def add(self, name, value):
#         # todo
#         if isinstance(value, (int, float, complex)):
#             formatted_value = str(value)
#         elif isinstance(value, str):
#             formatted_value = '"' + value + '"'
#         else:
#             formatted_value = ""
#
#         with open(self._netlist_filename, "at") as file:
#             file.write("%s=%s\n" % (name, formatted_value))
#
#     def add_block(self, block):
#         # todo
#         block = re.sub(r"\s+\\\s*\n", " ", block)
#         # block = re.sub(r"\\", r"\\\\", block)
#         with open(self._netlist_filename, "at") as file:
#             file.write("%s\n" % block)
#
#     def read_netlist(self, name):
#         try:
#             group, name = re.split(r"\.", name)
#         except ValueError:
#             group = ""
#         try:
#             # todo read value from xml
#             value = ''
#         except AttributeError:
#             logger.error("Unknown Netlist Variable: %s" % name, exc_info=True)
#             raise
#         if value[0] == '"' and value[-1] == '"':
#             return value[1:-1:1]
#         else:
#             value = value.replace("*j", "j").replace("j*", "1j*").replace("list", "").replace(")", ",)")
#             array_value = np.array(eval(value), dtype=np.complex128)
#             return array_value
#
#     def write(self, name, value, add_string_quotes=True):
#         try:
#             group, name = re.split(r"\.", name)
#         except ValueError:
#             group = ""
#         if isinstance(value, (int, float)):
#             formatted_value = str(value)
#         elif isinstance(value, str):
#             formatted_value = re.sub(r"\\", r"\\\\", value)
#             if add_string_quotes:
#                 formatted_value = '"' + formatted_value + '"'
#         else:
#             raise ValueError("Unsupported Netlist Variable Type: " + str(type(value)))
#
#         formatted_group = group
#         formatted_name = name
#         import pdb
#         pdb.set_trace()
#         # todo set a value in the xml file and write the netlist file
#
#     def import_iq(self, shell_options="", timeout=20):
#         # todo
#         if self.is_remote():
#             path, _ = os.path.split(self._remote_netlist_filename)
#             remote_netlist_filename = path + "load_iq.txt"
#             self._put_remote_file(remote_netlist_filename)
#             cmd = "hpeesofsim " + remote_netlist_filename + " " + shell_options
#             stdin_file, stdout_file, stderr_file = self._remote_ssh.exec_command(cmd, timeout=timeout)
#             time.sleep(3) # Wait for dataset file to be written
#             # Todo remove dataset generation delay
#             self._get_remote_file(self._remote_dataset_filename)
#             log = stdout_file.read()
#             result = -1 if stderr_file.read() else 0
#         else:
#             netlist_filename = self._netlist_filename
#             cmd = "hpeesofsim " + netlist_filename + " " + shell_options
#             log = subprocess.call(cmd, shell=True, timeout=timeout)
#             result = 0
#         if result < 0:
#             raise SimulatorError(log)
#         return result, log
#
#     def measure(self, shell_options="", timeout=50):
#         result = 0
#         log = ''
#         if self.is_remote():
#             raise NotImplementedError
#         else:
#             cmd = 'qucs-s -n -i ' + self._netlist_filename + ' -o ' + self._dataset_filename + '--xyce --run'
#             result = subprocess.call(cmd, shell=True, timeout=timeout)
#         if result < 0:
#             raise SimulatorError(log)
#         return result, log
#
#     def read(self, name, sweep_idx=slice(None), freq_idx=slice(None), time_idx=slice(None)):
#         data = QucsDataset(self._dataset_filename)
#         # data.variables()   # print all variables
#         y = data.results(name)
#         return y[sweep_idx, freq_idx, time_idx].reshape(sweep_idx.size, freq_idx.size, time_idx.size)


if __name__ == "__main__":
    PortBlock = \
        """
        <Components>
          <IProbe I_1H 1 100 -130 16 -26 0 1>
          <VProbe V_1H 1 130 -80 -15 -48 1 0>
          <BiasT X3 5 100 0 34 -26 0 1 "1 uH" 0 "1 uF" 0>
          <Vdc V_10 1 150 0 -26 18 1 2 "vm_10" 1>
          <GND * 1 170 -100 0 0 0 1>
          <Vac V11 1 130 110 -26 18 1 2 "vm_11" 1 "1e9" 1 "0" 1 "0" 0 "0" 0 "0" 0>
          <Vac V12 1 130 200 -26 18 0 0 "vm_12" 1 "2e9" 1 "90" 1 "0" 0 "0" 0 "0" 0>
          <Vac V13 1 130 300 -26 18 1 2 "vm_13" 1 "3e9" 1 "180" 1 "0" 0 "0" 0 "0" 0>
          <SPfile X5 1 130 400 -26 39 1 2 "G_1H.s1p" 1 "rectangular" 0 "linear" 0 "open" 0 "1" 0>
          <GND * 1 130 430 0 0 0 0>
        </Components>
        """


    import_schematic('schematic.sch.in', 'schematic.sch','schematic.xml')
    tree = ET.parse('schematic.xml')

    # Get the root element of the XML document
    root = tree.getroot()

    print(root)

    # ads = QucsSimulator("schematic.sch", "schematic.dat",
    #                    "HB", "HG1",
    #                    remote_host='', remote_user='', remote_key_filename='')
    # var1 = ads.read_netlist("NumericVar1")
    # ads.write("NumericVar1", 12)
    # var1_ = ads.read_netlist("NumericVar1")
    # result_, log_ = ads.measure()
    # meas1 = ads.read("Meas1")
