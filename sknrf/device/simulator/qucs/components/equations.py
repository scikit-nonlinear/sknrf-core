from dataclasses import dataclass

from sknrf.device.simulator.qucs.components.base import AbstractComponent
from sknrf.device.simulator.qucs.enums import ENABLE, MIRROR, ROTATION


@dataclass
class Eqn(AbstractComponent):
    """ Eqn Component

        Parameters:
        _label (str, optional): Schematic label. Defaults to 'Eqn'.

        Name (str): Part RefID.
        Enable (ENABLE): Part enable.
        x (int): X-Coordinate.
        y (int): Y-Coordinate.
        tx (int): X-Coordinate of Text-Box.
        ty (int): Y-Coordinate of Text-Box.
        mirror (MIRROR): Part mirror.
        rotation (ROTATION): Part rotation.
        f_11 (str): Frequency of {port}{1} in Hertz.
        f_11_display (bool): Display Frequency of {port}{1} in Hertz.
        f_12 (str): Frequency of {port}{2} in Hertz.
        f_12_display (bool): Display Frequency of {port}{2} in Hertz.
        f_13 (str): Frequency of {port}{3} in Hertz.
        f_13_display (bool): Display Frequency of {port}{3} in Hertz.
        vm_11 (str): Magnitude of Voltage of {port}{1} in Hertz.
        vm_11_display (bool): Display Magnitude of Voltage of {port}{1} in Hertz.
        vm_12 (str): Magnitude of Voltage of {port}{2} in Hertz.
        vm_12_display (bool): Display Magnitude of Voltage of {port}{2} in Hertz.
        vm_13 (str): Magnitude of Voltage of {port}{3} in Hertz.
        vm_13_display (bool): Display Magnitude of Voltage of {port}{3} in Hertz.
        vp_11 (str): Phase of Voltage of {port}{1} in Hertz.
        vp_11_display (bool): Display Phase of Voltage of {port}{1} in Hertz.
        vp_12 (str): Phase of Voltage of {port}{2} in Hertz.
        vp_12_display (bool): Display Phase of Voltage of {port}{2} in Hertz.
        vp_13 (str): Phase of Voltage of {port}{3} in Hertz.
        vp_13_display (bool): Display Phase of Voltage of {port}{3} in Hertz.
        Export (str): Export result into dataset [yes, no].
        Export_display (bool): Display Export result into dataset [yes, no].
    """
    _label = 'Eqn'

    Name: str
    Enable: ENABLE
    x: int
    y: int
    tx: int
    ty: int
    mirror: MIRROR
    rotation: ROTATION
    f_11: str
    f_11_display: bool
    f_12: str
    f_12_display: bool
    f_13: str
    f_13_display: bool
    vm_10: str
    vm_10_display: bool
    vm_11: str
    vm_11_display: bool
    vm_12: str
    vm_12_display: bool
    vm_13: str
    vm_13_display: bool
    vp_10: str
    vp_10_display: bool
    vp_11: str
    vp_11_display: bool
    vp_12: str
    vp_12_display: bool
    vp_13: str
    vp_13_display: bool
    Export: str
    Export_display: bool
