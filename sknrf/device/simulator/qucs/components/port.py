from dataclasses import dataclass

from sknrf.device.simulator.qucs.components.base import AbstractComponent
from sknrf.device.simulator.qucs.enums import ENABLE, MIRROR, ROTATION


@dataclass
class PortOffset:
    """ Port Offset

        IProbe (int): Offset from Port tag to IProbe tag.
        VProbe (int): Offset from Port tag to VProbe tag.
        BIAS_T (int): Offset from Port tag to BIAS_T tag.
        Vdc (int): Offset from Port tag to Vdc tag.
        GND_DC (int): Offset from Port tag to GND_DC tag.
        Vac1 (int): Offset from Port tag to Vac1 tag.
        Vac2 (int): Offset from Port tag to Vac2 tag.
        Vac3 (int): Offset from Port tag to Vac3 tag.
        SPfile (int): Offset from Port tag to SPfile tag.
        GND_AC (int): Offset from Port tag to GND_AC tag.
        Eqn (int): Offset from Port tag to Eqn tag.
    """
    IProbe: int = 1
    VProbe: int = 2
    BIAS_T: int = 3
    Vdc: int = 4
    GND_DC: int = 5
    Vac1: int = 6
    Vac2: int = 7
    Vac3: int = 8
    SPfile: int = 9
    GND_AC: int = 10
    Eqn: int = 11


@dataclass
class Port(AbstractComponent):
    """ Port Symbol

        Parameters:
        _label (str, optional): Schematic label. Defaults to 'Port'.

        Name (str): Port RefID.
        Enable (ENABLE): Part enable.
        x (int): X-Coordinate.
        y (int): Y-Coordinate.
        tx (int): X-Coordinate of Text-Box.
        ty (int): Y-Coordinate of Text-Box.
        mirror (MIRROR): Part mirror.
        rotation (ROTATION): Part rotation.
        Num (str): Port Number.
        Num_display (bool): Display Port Number.
        Type (str): Port Type [analog, in, out, inout].
        Type_display (bool): Display Port Type [analog, in, out, inout].
        XSPICE_Type (str): Port XSPICE Type [v, i, vd, id, h, g, hd, gd].
        XSPICE_Type_display (bool): Display Port XSPICE Type [v, i, vd, id, h, g, hd, gd].
        Bounded (str): Conjugated Port for XSPICE Differential Ports.
        Bounded_display (bool): Display Conjugated Port for XSPICE Differential Ports.
    """
    _label = 'Port'

    Name: str
    Enable: ENABLE
    x: int
    y: int
    tx: int
    ty: int
    mirror: MIRROR
    rotation: ROTATION
    Num: str
    Num_display: int
    Type: str
    Type_display: int
    XSPICE_Type: str
    XSPICE_Type_display: int
    Bounded: str
    Bounded_display: int
