from dataclasses import dataclass

from sknrf.device.simulator.qucs.components.base import AbstractComponent
from sknrf.device.simulator.qucs.enums import ENABLE, MIRROR, ROTATION


@dataclass
class Vdc(AbstractComponent):
    """ Vdc Component

        Parameters:
        _label (str, optional): Schematic label. Defaults to 'Vdc'.

        Name (str): Part RefID.
        Enable (ENABLE): Part enable.
        x (int): X-Coordinate.
        y (int): Y-Coordinate.
        tx (int): X-Coordinate of Text-Box.
        ty (int): Y-Coordinate of Text-Box.
        mirror (MIRROR): Part mirror.
        rotation (ROTATION): Part rotation.
        U (str): DC Voltage in Volts
        U_display (bool): Display DC Voltage in Volts
    """
    _label = 'Vdc'

    Name: str
    Enable: ENABLE
    x: int
    y: int
    tx: int
    ty: int
    mirror: MIRROR
    rotation: ROTATION
    U: str
    U_display: int


@dataclass
class Vac(AbstractComponent):
    """ Vac Component

        Parameters:
        _label (str, optional): Schematic label. Defaults to 'Vac'.

        Name (str): Part RefID.
        Enable (ENABLE): Part enable.
        x (int): X-Coordinate.
        y (int): Y-Coordinate.
        tx (int): X-Coordinate of Text-Box.
        ty (int): Y-Coordinate of Text-Box.
        mirror (MIRROR): Part mirror.
        rotation (ROTATION): Part rotation.
        U (str): RF Voltage in Volts.
        U_display (bool): Display RF Voltage in Volts.
        f (str): RF Frequency in Hertz.
        f_display (bool): Display RF Frequency in Hertz.
        Phase (str): RF Phase in Degrees.
        PHase_display (bool): Display RF Phase in Degrees.
        Theta (str): RF Damping Factor.
        Theta_display (bool): Display RF Damping Factor.
        VO (str): RF Voltage Offset in Volts.
        VO_display (bool): Display RF Voltage Offset in Volts.
        TO (str): RF Time Offset in s.
        TO_display (bool): Display RF Time Offset in s.
    """
    _label = 'Vac'

    Name: str
    Enable: ENABLE
    x: int
    y: int
    tx: int
    ty: int
    mirror: MIRROR
    rotation: ROTATION
    U: str
    U_display: int
    f: str
    f_display: int
    Phase: str
    Phase_display: int
    Theta: str
    Theta_display: int
    V0: str
    V0_display: int
    TD: str
    TD_display: int
