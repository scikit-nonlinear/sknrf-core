import re
from enum import Enum
from abc import ABC
from dataclasses import astuple
from typing import List


class AbstractComponent(ABC, dict):
    """ Abstract Group

        Parameters:
        _label (str, optional): Schematic label. Defaults to ''.
        _pattern (str, optional): Schematic regex. Defaults to '(?:[^\\s"]+|\"[^\"]*\")'.
        _title_sep (str, optional): Character that separates title from attributes. Defaults to ' '.
        _attr_sep (str, optional): Character that separates attributes. Defaults to ' '.
        _hidden (bool, optional): If True, don't show tag in .sch file. Defaults to 'False'.
    """
    _label = ''
    _pattern = r'(?:[^\s"]+|\"[^\"]*\")'
    _title_sep = ' '
    _attr_sep = ' '
    _hidden = False  # Don't show tag in .sch file


    @classmethod
    def parse(cls, text) -> List:
        """  Parse the Unformatted XML-like tag
            - [ ^\\s"]+: Matches a sequence of characters that are neither spaces nor double quotes. This part will match "normal" words.
            - \"[^\"]*\": Matches any sequence of characters enclosed by double quotes. [^"]* ensures that it matches any characters except for double quotes inside the quotes.

        Parameters
        ----------
        text

        Returns
        -------

        """
        text = text.lstrip('<').rstrip('>')
        attr_start = text.index(cls._title_sep) + len(cls._title_sep)
        attrs = text[attr_start::]
        args = re.findall(cls._pattern, attrs)

        # Convert from str to type
        typed_args = []
        for k, v in zip(cls.__annotations__.keys(), args):
            t = cls.__annotations__[k]
            try:
                if t is int:
                    v = v.lstrip('"').rstrip('"')
                    typed_args.append(t(v))
                elif t is float:
                    v = v.lstrip('"').rstrip('"')
                    typed_args.append(t(v))
                elif t is str:
                    if '=' in v:
                        v = v[v.index('=') + 1:]
                    v = v.lstrip('"').rstrip('"')
                    typed_args.append(t(v))
                elif t is bool:
                    typed_args.append(t(int(v)))
                elif t.__base__ is Enum:
                    typed_args.append(t(int(v)))
                else:
                    raise TypeError(f"Unsupported type: {str(t):s}")
            except ValueError as e:
                raise ValueError(f'{cls.__name__:s}.{k:s} = {str(t):s}({v:s}): {e}') from e
        return typed_args

    def xml_tag(self, indent=0):
        indent_str = ' ' * 2 * indent

        # Convert from type to str
        str_args = []
        for k in self.__annotations__.keys():
            t = self.__annotations__[k]
            if t is int:
                str_args.append(f'{k:s}="{self.__getattribute__(k):d}"')
            elif t is float:
                str_args.append(f'{k:s}="{self.__getattribute__(k):.3g}"')
            elif t is str:
                str_args.append(f'{k:s}="{self.__getattribute__(k):s}"')
            elif t is bool:
                str_args.append(f'{k:s}="{self.__getattribute__(k):d}"')
            elif t.__base__ is Enum:
                str_args.append(f'{k:s}="{self.__getattribute__(k).value:d}"')
            else:
                raise TypeError(f"Unsupported type: {str(t):s}")

        xml_str = ' '.join(str_args)
        xml_str = f'{self.__class__.__name__:s} {xml_str:s}'
        tag = f'{indent_str:s}<{xml_str:s}/>\n'
        return tag

    def sch_tag(self, indent=0):
        indent_str = ' ' * 2 * indent

        # Convert from type to str
        str_args = []
        for k in self.__annotations__.keys():
            t = self.__annotations__[k]
            if t is int:
                str_args.append(f'{self.__getattribute__(k):d}')
            elif t is float:
                str_args.append(f'{self.__getattribute__(k):.3g}')
            elif t is str:
                if k == 'Name':
                    str_args.append(f'{self.__getattribute__(k):s}')
                elif self.__class__.__name__ == 'Eqn' and k not in ('Export',):
                    str_args.append(f'"{k:s}={self.__getattribute__(k):s}"')
                else:
                    str_args.append(f'"{self.__getattribute__(k):s}"')
            elif t is bool:
                str_args.append(f'{self.__getattribute__(k):d}')
            elif t.__base__ is Enum:
                str_args.append(f'{self.__getattribute__(k).value:d}')
            else:
                raise TypeError(f"Unsupported type: {str(t):s}")

        sch_str = self._attr_sep.join(str_args)
        sch_str = self._label + self._title_sep + sch_str
        tag = f'' if self._hidden else f'{indent_str:s}<{sch_str:s}>\n'
        return tag
