from dataclasses import dataclass

from sknrf.device.simulator.qucs.components.base import AbstractComponent
from sknrf.device.simulator.qucs.enums import ENABLE, MIRROR, ROTATION


@dataclass
class SW(AbstractComponent):
    """ SW Component

        Parameters:
        _label (str, optional): Schematic label. Defaults to '.SW'.

        Name (str): Part RefID.
        Enable (ENABLE): Part enable.
        x (int): X-Coordinate.
        y (int): Y-Coordinate.
        tx (int): X-Coordinate of Text-Box.
        ty (int): Y-Coordinate of Text-Box.
        mirror (MIRROR): Part mirror.
        rotation (ROTATION): Part rotation.
        Simulation (str): Simulation Component Name.
        Simulation_display (bool): Display Simulation Component Name.
        Type (str): Solver type [linear, logarithmic, list, constant].
        Type_display (bool): Display Solver type [lin, log, list, constant].
        Parameter (str): Sweep Parameter.
        Parameter_display (bool): Display Sweep Parameter.
        Start (str): Start.
        Start_display (bool): Display Start.
        Stop (str): Stop.
        Stop_display (bool): Display Stop.
        Number (str): Number of points.
        Number_display (bool): Display Number of points.
    """
    _label = '.SW'

    Name: str
    Enable: ENABLE
    x: int
    y: int
    tx: int
    ty: int
    mirror: MIRROR
    rotation: ROTATION
    Simulation: str
    Simulation_display: int
    Type: str
    Type_display: bool
    Parameter: str
    Parameter_display: bool
    Start: str
    Start_display: bool
    Stop: str
    Stop_display: bool
    Number: str
    Number_display: bool


@dataclass
class DC(AbstractComponent):
    """ DC Component

        Parameters:
        _label (str, optional): Schematic label. Defaults to '.DC'.

        Name (str): Part RefID.
        Enable (ENABLE): Part enable.
        x (int): X-Coordinate.
        y (int): Y-Coordinate.
        tx (int): X-Coordinate of Text-Box.
        ty (int): Y-Coordinate of Text-Box.
        mirror (MIRROR): Part mirror.
        rotation (ROTATION): Part rotation.
        Temp (str): Simulation Temperature in Degree Celsius.
        Temp_display (bool): Display Simulation Temperature in Degree Celsius.
        reltol (str): Relative Tolerance for Convergence.
        reltol_display (bool): Display Relative Tolerance for Convergence.
        abstol (str): Absolute Tolerance for Currents in A.
        abstol_display (bool): Display Absolute Tolerance for Currents in A.
        vntol (str): Absolute Tolerance for Voltages in V.
        vntol_display (bool): Display Absolute Tolerance for Voltages in V.
        saveOPs (str): Put Operating Points into Dataset [yes, no].
        saveOPs_display (bool): Display Put Operating Points into Dataset [yes, no].
        MaxIter (str): Maximum number of iterations until Error.
        MaxIter_display (bool): Display Maximum number of iterations until Error.
        saveAll (str): Save Sub-circuit Characteristic Values into Dataset [yes, no].
        saveAll_display (bool): Display Save Sub-circuit Characteristic Values into Dataset [yes, no].
        convHelper (str): Preferred Convergence Algorithm [none, gMinStepping, SteepestDecent, LineSearch, Attenuation, SourceStepping].
        convHelper_display (bool): Display Preferred Convergence Algorithm [none, gMinStepping, SteepestDecent, LineSearch, Attenuation, SourceStepping].
        Solver (str): Method for solving the circuit matrix [CroutLU, DoolittleLU, HouseholderQR, HouseholderLQ, GolubSVD].
        Solver_display (bool): Display Method for solving the circuit matrix [CroutLU, DoolittleLU, HouseholderQR, HouseholderLQ, GolubSVD].
    """
    _label = '.DC'

    Name: str
    Enable: ENABLE
    x: int
    y: int
    tx: int
    ty: int
    mirror: MIRROR
    rotation: ROTATION
    Temp: str
    Temp_display: bool
    reltol: str
    reltol_display: bool
    abstol: str
    abstol_display: bool
    vntol: str
    vntol_display: bool
    saveOPs: str
    saveOPs_display: bool
    MaxIter: str
    MaxIter_display: bool
    saveAll: str
    saveAll_display: bool
    convHelper: str
    convHelper_display: bool
    Solver: str
    Solver_display: bool


@dataclass
class AC(AbstractComponent):
    """ AC Component

        Parameters:
        _label (str, optional): Schematic label. Defaults to '.AC'.

        Name (str): Part RefID.
        Enable (ENABLE): Part enable.
        x (int): X-Coordinate.
        y (int): Y-Coordinate.
        tx (int): X-Coordinate of Text-Box.
        ty (int): Y-Coordinate of Text-Box.
        mirror (MIRROR): Part mirror.
        rotation (ROTATION): Part rotation.
        Type (str): Solver type [linear, logarithmic, list, constant].
        Type_display (bool): Display Solver type [lin, log, list, constant].
        Start (str): Start Frequency in Hz.
        Start_display (bool): Display Start Frequency in Hz.
        Stop (str): Stop Frequency in Hz.
        Stop_display (bool): Display Stop Frequency in Hz.
        Number (str): Number of points in time.
        Number_display (bool): Display Number of points in time.
        Noise (str): Calculate the noise parameters [yes, no].
        Noise_display (bool): Display Calculate the noise parameters [yes, no].
    """
    _label = '.AC'

    Name: str
    Enable: ENABLE
    x: int
    y: int
    tx: int
    ty: int
    mirror: MIRROR
    rotation: ROTATION
    Type: str
    Type_display: bool
    Start: str
    Start_display: bool
    Stop: str
    Stop_display: bool
    Number: str
    Number_display: bool
    Noise: str
    Noise_display: bool


@dataclass
class SP(AbstractComponent):
    """ SP Component

        Parameters:
        _label (str, optional): Schematic label. Defaults to '.HB'.

        Name (str): Part RefID.
        Enable (ENABLE): Part enable.
        x (int): X-Coordinate.
        y (int): Y-Coordinate.
        tx (int): X-Coordinate of Text-Box.
        ty (int): Y-Coordinate of Text-Box.
        mirror (MIRROR): Part mirror.
        rotation (ROTATION): Part rotation.
        Type (str): Solver type [linear, logarithmic, list, constant].
        Type_display (bool): Display Solver type [lin, log, list, constant].
        Start (str): Start Frequency in Hz.
        Start_display (bool): Display Start Frequency in Hz.
        Stop (str): Stop Frequency in Hz.
        Stop_display (bool): Display Stop Frequency in Hz.
        Number (str): Number of points in time.
        Number_display (bool): Display Number of points in time.
        Noise (str): Calculate the noise parameters [yes, no].
        Noise_display (bool): Display Calculate the noise parameters [yes, no].
        NoiseIP (str): Input Port for Noise Figure.
        NoiseIP_display (bool): Display Input Port for Noise Figure.
        NoiseOP (str): Output Port for Noise Figure.
        NoiseOP_display (bool): Display Output Port for Noise Figure.
        saveCVs (str): Put Characteristic Values into Dataset [yes, no].
        saveCVs_display (bool): Display Put Characteristic Values into Dataset [yes, no].
        saveAll (str): Save Sub-circuit Characteristic Values into Dataset [yes, no].
        saveAll_display (bool): Display Save Sub-circuit Characteristic Values into Dataset [yes, no].
    """
    _label = '.SP'

    Name: str
    Enable: ENABLE
    x: int
    y: int
    tx: int
    ty: int
    mirror: MIRROR
    rotation: ROTATION
    Type: str
    Type_display: bool
    Start: str
    Start_display: bool
    Stop: str
    Stop_display: bool
    Number: str
    Number_display: bool
    Noise: str
    Noise_display: bool
    NoiseIP: str
    NoiseIP_display: bool
    NoiseOP: str
    NoiseOP_display: bool
    saveCVs: str
    saveCVs_display: bool
    saveAll: str
    saveAll_display: bool


@dataclass
class NOISE(AbstractComponent):
    """ NOISE Component

        Parameters:
        _label (str, optional): Schematic label. Defaults to '.NOISE'.

        Name (str): Part RefID.
        Enable (ENABLE): Part enable.
        x (int): X-Coordinate.
        y (int): Y-Coordinate.
        tx (int): X-Coordinate of Text-Box.
        ty (int): Y-Coordinate of Text-Box.
        mirror (MIRROR): Part mirror.
        rotation (ROTATION): Part rotation.
        Type (str): Solver type [linear, logarithmic, list, constant].
        Type_display (bool): Display Solver type [lin, log, list, constant].
        Start (str): Start Frequency in Hz.
        Start_display (bool): Display Start Frequency in Hz.
        Stop (str): Stop Frequency in Hz.
        Stop_display (bool): Display Stop Frequency in Hz.
        Number (str): Number of points in time.
        Number_display (bool): Display Number of points in time.
        Output (str): Node at which the Total Output is Desired.
        Output_display (bool): Display Node at which the Total Output is Desired.
        Source (str): Independent Source to which Input Noise is Referred.
        Source_display (bool): Display Independent Source to which Input Noise is Referred.
    """
    _label = '.NOISE'

    Name: str
    Enable: ENABLE
    x: int
    y: int
    tx: int
    ty: int
    mirror: MIRROR
    rotation: ROTATION
    Type: str
    Type_display: bool
    Start: str
    Start_display: bool
    Stop: str
    Stop_display: bool
    Number: str
    Number_display: bool
    Output: str
    Output_display: bool
    Source: str
    Source_display: bool


@dataclass
class HB(AbstractComponent):
    """ HB Component

        Parameters:
        _label (str, optional): Schematic label. Defaults to '.HB'.

        Name (str): Part RefID.
        Enable (ENABLE): Part enable.
        x (int): X-Coordinate.
        y (int): Y-Coordinate.
        tx (int): X-Coordinate of Text-Box.
        ty (int): Y-Coordinate of Text-Box.
        mirror (MIRROR): Part mirror.
        rotation (ROTATION): Part rotation.
        f (str): RF Frequency in Hertz.
        f_display (bool): Display RF Frequency in Hertz.
        n (str): Number of harmonics.
        n_display (bool): Display Number of harmonics.
        iabstol (str): Absolute Tolerance for Currents.
        iabstol_display (bool): Display Absolute Tolerance for Currents.
        vabstol (str): Absolute Tolerance for Voltages.
        vabstol_display (bool): Display Absolute Tolerance for Voltages.
        reltol (str): Relative Tolerance for Convergence.
        reltol_display (bool): Display Relative Tolerance for Convergence.
        MaxIter (str): Maximum number of iterations until Error.
        MaxIter_display (bool): Display Maximum number of iterations until Error.
    """
    _label = '.HB'

    Name: str
    Enable: ENABLE
    x: int
    y: int
    tx: int
    ty: int
    mirror: MIRROR
    rotation: ROTATION
    f: str
    f_display: bool
    n: str
    n_display: bool
    iabstol: str
    iabstol_display: bool
    vabstol: str
    vabstol_display: bool
    reltol: str
    reltol_display: bool
    MaxIter: str
    MaxIter_display: bool


@dataclass
class TR(AbstractComponent):
    """ TR Component

        Parameters:
        _label (str, optional): Schematic label. Defaults to '.TR'.

        Name (str): Part RefID.
        Enable (ENABLE): Part enable.
        x (int): X-Coordinate.
        y (int): Y-Coordinate.
        tx (int): X-Coordinate of Text-Box.
        ty (int): Y-Coordinate of Text-Box.
        mirror (MIRROR): Part mirror.
        rotation (ROTATION): Part rotation.
        Type (str): Solver type [linear, logarithmic, list, constant].
        Type_display (bool): Display Solver type [lin, log, list, constant].
        Start (float): Start time in s.
        Start_display (bool): Display Start time in s.
        Stop (float): Stop time in s.
        Stop_display (bool): Display Stop time in s.
        Number (int): Number of points in time.
        Number_display (bool): Display Number of points in time.
        IntegrationMethod (str): Integration Method [Euler, Trapezoid, Gear, AdamsMoulton].
        IntegrationMethod_display (bool): Display Integration Method [Euler, Trapezoid, Gear, AdamsMoulton].
        Order (int): Order of Integration Method [1-6].
        Order_display (bool): Order of Integration Method [1-6].
        InitialStep (float): Initial Step Size in s.
        InitialStep_display (bool): Display Initial Step Size in s.
        MinStep (float): Minimum Step Size in s.
        MinStep_display (bool): Display Minimum Step Size in s.
        MaxIter (int): Maximum Number of Iterations Until Error.
        MaxIter_display (bool): Display Maximum Number of Iterations Until Error.
        reltol (float): Relative Tolerance for Convergence.
        reltol_display (bool): Display Relative Tolerance for Convergence.
        abstol (float): Absolute Tolerance for Currents in A.
        abstol_display (bool): Display Absolute Tolerance for Currents in A.
        vntol (float): Absolute Tolerance for Voltages in V.
        vntol_display (bool): Display Absolute Tolerance for Voltages in V.
        Temp (float): Simulation Temperature in Degree Celsius.
        Temp_display (bool): Display Simulation Temperature in Degree Celsius.
        LTEreltol (float): Relative Tolerance of Local Truncation Error.
        LTEreltol_display (bool): Relative Tolerance of Local Truncation Error.
        LTEabstol (float): Absolute Tolerance of Local Truncation Error.
        LTEabstol_display (bool): Display Absolute Tolerance of Local Truncation Error.
        LTEfactor (float): Overestimation of Local Truncation Error.
        LTEfactor_display (bool): Display Overestimation of Local Truncation Error.
        Solver (str): Method for solving the circuit matrix [CroutLU, DoolittleLU, HouseholderQR, HouseholderLQ, GolubSVD].
        Solver_display (bool): Display Method for solving the circuit matrix [CroutLU, DoolittleLU, HouseholderQR, HouseholderLQ, GolubSVD].
        relaxTSR (str): Relax Time Raster [no, yes].
        relaxTSR_display (bool): Display Relax Time Raster [no, yes].
        initialDC (str): Perform Initial DC (set 'no' to activate UIC) [yes, no].
        initialDC_display (bool): Display Perform Initial DC (set 'no' to activate UIC) [yes, no].
        MaxStep (int): Maximum Step Size in s.
        MaxStep_display (bool): Display Maximum Step Size in s.
    """
    _label = '.TR'

    Name: str
    Enable: ENABLE
    x: int
    y: int
    tx: int
    ty: int
    mirror: MIRROR
    rotation: ROTATION
    Type: str
    Type_display: bool
    Start: float
    Start_display: bool
    Stop: float
    Stop_display: bool
    Number: int
    Number_display: bool
    IntegrationMethod: str
    IntegrationMethod_display: bool
    Order: int
    Order_display: bool
    InitialStep: float
    InitialStep_display: bool
    MinStep: float
    MinStep_display: bool
    MaxIter: int
    MaxIter_display: bool
    reltol: float
    reltol_display: bool
    abstol: float
    abstol_display: bool
    vntol: float
    vntol_display: bool
    Temp: float
    Temp_display: bool
    LTEreltol: float
    LTEreltol_display: bool
    LTEabstol: float
    LTEabstol_display: bool
    LTEfactor: float
    LTEfactor_display: bool
    Solver: str
    Solver_display: bool
    relaxTSR: str
    relaxTSR_display: bool
    initialDC: str
    initialDC_display: bool
    MaxStep: int
    MaxStep_display:bool


@dataclass
class Digi(AbstractComponent):
    """ Digi Component

        Parameters:
        _label (str, optional): Schematic label. Defaults to '.Digi'.

        Name (str): Part RefID.
        Enable (ENABLE): Part enable.
        x (int): X-Coordinate.
        y (int): Y-Coordinate.
        tx (int): X-Coordinate of Text-Box.
        ty (int): Y-Coordinate of Text-Box.
        mirror (MIRROR): Part mirror.
        rotation (ROTATION): Part rotation.
        Type (str): Solver type [linear, logarithmic, list, constant].
        Type_display (bool): Display Solver type [lin, log, list, constant].
        time (str): Duration of TimeList Simulation.
        time_display (bool): Display Duration of TimeList Simulation.
        Model (str): Netlist Format [VHDL, Verilog].
        Model_display (bool): Display Netlist Format [VHDL, Verilog].
    """
    _label = '.Digi'

    Name: str
    Enable: ENABLE
    x: int
    y: int
    tx: int
    ty: int
    mirror: MIRROR
    rotation: ROTATION
    Type: str
    Type_display: bool
    time: str
    time_display: bool
    Model: str
    Model_display: bool
