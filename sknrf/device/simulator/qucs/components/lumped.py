from dataclasses import dataclass

from sknrf.device.simulator.qucs.components.base import AbstractComponent
from sknrf.device.simulator.qucs.enums import ENABLE, MIRROR, ROTATION


@dataclass
class GND(AbstractComponent):
    """ GND Component

        Parameters:
        _label (str, optional): Schematic label. Defaults to 'GND'.

        Name (str): Part RefID. Always equal to '*'
        Enable (ENABLE): Part enable.
        x (int): X-Coordinate.
        y (int): Y-Coordinate.
        tx (int): X-Coordinate of Text-Box.
        ty (int): Y-Coordinate of Text-Box.
        mirror (MIRROR): Part mirror.
        rotation (ROTATION): Part rotation.
    """
    _label = 'GND'

    Name: str
    Enable: ENABLE
    x: int
    y: int
    tx: int
    ty: int
    mirror: MIRROR
    rotation: ROTATION
