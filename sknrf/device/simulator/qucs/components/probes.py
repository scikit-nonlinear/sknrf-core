from dataclasses import dataclass

from sknrf.device.simulator.qucs.components.base import AbstractComponent
from sknrf.device.simulator.qucs.enums import ENABLE, MIRROR, ROTATION


@dataclass
class IProbe(AbstractComponent):
    """ IProbe Component

        Parameters:
        _label (str, optional): Schematic label. Defaults to 'IProbe'.

        Name (str): Part RefID.
        Enable (ENABLE): Part enable.
        x (int): X-Coordinate.
        y (int): Y-Coordinate.
        tx (int): X-Coordinate of Text-Box.
        ty (int): Y-Coordinate of Text-Box.
        mirror (MIRROR): Part mirror.
        rotation (ROTATION): Part rotation.
    """
    _label = 'IProbe'

    Name: str
    Enable: ENABLE
    x: int
    y: int
    tx: int
    ty: int
    mirror: MIRROR
    rotation: ROTATION


@dataclass
class VProbe(AbstractComponent):
    """ VProbe Component

        Parameters:
        _label (str, optional): Schematic label. Defaults to 'VProbe'.

        Name (str): Part RefID.
        Enable (ENABLE): Part enable.
        x (int): X-Coordinate.
        y (int): Y-Coordinate.
        tx (int): X-Coordinate of Text-Box.
        ty (int): Y-Coordinate of Text-Box.
        mirror (MIRROR): Part mirror.
        rotation (ROTATION): Part rotation.
    """
    _label = 'VProbe'

    Name: str
    Enable: ENABLE
    x: int
    y: int
    tx: int
    ty: int
    mirror: MIRROR
    rotation: ROTATION
