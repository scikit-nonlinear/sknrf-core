from dataclasses import dataclass

from sknrf.device.simulator.qucs.components.base import AbstractComponent
from sknrf.device.simulator.qucs.enums import ENABLE, MIRROR, ROTATION


@dataclass
class SPfile(AbstractComponent):
    """ SPfile Component

        Parameters:
        _label (str, optional): Schematic label. Defaults to 'SPfile'.

        Name (str): Part RefID.
        Enable (ENABLE): Part enable.
        x (int): X-Coordinate.
        y (int): Y-Coordinate.
        tx (int): X-Coordinate of Text-Box.
        ty (int): Y-Coordinate of Text-Box.
        mirror (MIRROR): Part mirror.
        rotation (ROTATION): Part rotation.
        File (str): Name of S-Parameter File.
        File_display (bool): Display Name of S-Parameter File.
        Data (str): Data type [rectangular, polar].
        Date_display (bool): Display Data type [rectangular, polar].
        Interpolator (str): Interpolation type [linear, cubic].
        Interpolator_display (bool): Display Interpolation type [linear, cubic].
        duringDC (str): Representation During DC Analysis type [open, short, shortall, unspecified].
        duringDC_display (bool): Display Representation During DC Analysis type [open, short, shortall, unspecified].
        Ports (str): Number of Ports.
        Ports_display (bool): Display Number of Ports.
    """
    _label = 'SPfile'

    Name: str
    Enable: ENABLE
    x: int
    y: int
    tx: int
    ty: int
    mirror: MIRROR
    rotation: ROTATION
    File: str
    File_display: int
    Data: str
    Data_display: int
    Interpolator: str
    Interpolator_display: int
    duringDC: str
    duringDC_display: int
    Ports: str
    Ports_display: int
