from dataclasses import dataclass

from sknrf.device.simulator.qucs.components.base import AbstractComponent
from sknrf.device.simulator.qucs.enums import ENABLE, MIRROR, ROTATION


@dataclass
class BiasT(AbstractComponent):
    """ BiasT Component

        Parameters:
        _label (str, optional): Schematic label. Defaults to 'BiasT'.

        Name (str): Part RefID.
        Enable (ENABLE): Part enable.
        x (int): X-Coordinate.
        y (int): Y-Coordinate.
        tx (int): X-Coordinate of Text-Box.
        ty (int): Y-Coordinate of Text-Box.
        u0 (int): Unknown Parameter 0.
        mirror (MIRROR): Part mirror.
        rotation (ROTATION): Part rotation.
        L (str): RF Inductance in Henry.
        L_display (bool): Display RF Inductance in Henry.
        C (str): DC Capacitance in Farad.
        C_display (bool): Display DC Capacitance in Farad.
    """
    _label = 'BiasT'

    Name: str
    x: int
    y: int
    tx: int
    ty: int
    u0: int
    mirror: MIRROR
    rotation: ROTATION
    L: str
    L_display: int
    C: str
    C_display: int
