from dataclasses import dataclass

from sknrf.device.simulator.qucs.components.base import AbstractComponent


@dataclass
class View(AbstractComponent):
    """ View Property

        Parameters:
        _label (str, optional): Schematic label. Defaults to 'View'.
        _pattern (str, optional): Schematic regex. Defaults to '(?:[^\\,"]+|\"[^\"]*\")'.
        _title_sep (str, optional): Character that separates title from attributes. Defaults to '='.
        _attr_sep (str, optional): Character that separates attributes. Defaults to ','.

        x0 (int): x-coordinate of top left corner of view.
        y0 (int): y-coordinate of top left corner of view.
        x1 (int): x-coordinate of bottom right corner of view.
        y1 (int): y-coordinate of bottom right corner of view.
        zoom (int): Zoom level of view.
        x_shift (int): Shift of x-coordinate of view to include schematic.
        y_shift (int): Shift of y-coordinate of view to include schematic.
    """
    _label = 'View'
    _pattern = r'(?:[^\,"]+|\"[^\"]*\")'
    _title_sep = '='
    _attr_sep = ','

    x0: int
    y0: int
    x1: int
    y1: int
    zoom: float
    x_shift: int  # How to shift view to see circuit
    y_shift: int  # How to shift view to see circuit


@dataclass
class Grid(AbstractComponent):
    """ Grid Property

        Parameters:
        _label (str, optional): Schematic label. Defaults to 'View'.
        _pattern (str, optional): Schematic regex. Defaults to '(?:[^\\,"]+|\"[^\"]*\")'.
        _title_sep (str, optional): Character that separates title from attributes. Defaults to '='.
        _attr_sep (str, optional): Character that separates attributes. Defaults to ','.

        u0 (int): Unknown parameter 0.
        u1 (int): Unknown parameter 1.
        u2 (int): Unknown parameter 2.
    """
    _label = 'Grid'
    _pattern = r'(?:[^\,"]+|\"[^\"]*\")'
    _title_sep = '='
    _attr_sep = ','

    u0: int
    u1: int
    u2: int


@dataclass
class DataSet(AbstractComponent):
    """ DataSet Property

        Parameters:
        _label (str, optional): Schematic label. Defaults to 'DataSet'.
        _pattern (str, optional): Schematic regex. Defaults to '(?:[^\\,"]+|\"[^\"]*\")'.
        _title_sep (str, optional): Character that separates title from attributes. Defaults to '='.
        _attr_sep (str, optional): Character that separates attributes. Defaults to ','.

        Name (str): Dataset name (.dat file)
    """
    _label = 'DataSet'
    _pattern = r'(?:[^\,"]+|\"[^\"]*\")'
    _title_sep = '='
    _attr_sep = ','

    Name: str


@dataclass
class DataDisplay(AbstractComponent):
    """ DataSet Property

        Parameters:
        _label (str, optional): Schematic label. Defaults to 'DataDisplay'.
        _pattern (str, optional): Schematic regex. Defaults to '(?:[^\\,"]+|\"[^\"]*\")'.
        _title_sep (str, optional): Character that separates title from attributes. Defaults to '='.
        _attr_sep (str, optional): Character that separates attributes. Defaults to ','.

        Name (str): DataDisplay name (.dpl file)
    """
    _label = 'DataDisplay'
    _pattern = r'(?:[^\,"]+|\"[^\"]*\")'
    _title_sep = '='
    _attr_sep = ','

    Name: str


@dataclass
class OpenDisplay(AbstractComponent):
    """ OpenDisplay Property

        Parameters:
        _label (str, optional): Schematic label. Defaults to 'OpenDisplay'.
        _pattern (str, optional): Schematic regex. Defaults to '(?:[^\\,"]+|\"[^\"]*\")'.
        _title_sep (str, optional): Character that separates title from attributes. Defaults to '='.
        _attr_sep (str, optional): Character that separates attributes. Defaults to ','.

        OpenDisplay (int): Open Display after simulation
    """
    _label = 'OpenDisplay'
    _pattern = r'(?:[^\,"]+|\"[^\"]*\")'
    _title_sep = '='
    _attr_sep = ','

    OpenDisplay: int


@dataclass
class Script(AbstractComponent):
    """ Script Property

        Parameters:
        _label (str, optional): Schematic label. Defaults to 'Script'.
        _pattern (str, optional): Schematic regex. Defaults to '(?:[^\\,"]+|\"[^\"]*\")'.
        _title_sep (str, optional): Character that separates title from attributes. Defaults to '='.
        _attr_sep (str, optional): Character that separates attributes. Defaults to ','.

        Name (str): Matlab/Octave Script name (.m file)
    """
    _label = 'Script'
    _pattern = r'(?:[^\,"]+|\"[^\"]*\")'
    _title_sep = '='
    _attr_sep = ','

    Name: str


@dataclass
class RunScript(AbstractComponent):
    """ RunScript Property

        Parameters:
        _label (str, optional): Schematic label. Defaults to 'RunScript'.
        _pattern (str, optional): Schematic regex. Defaults to '(?:[^\\,"]+|\"[^\"]*\")'.
        _title_sep (str, optional): Character that separates title from attributes. Defaults to '='.
        _attr_sep (str, optional): Character that separates attributes. Defaults to ','.

        RunScript (int): Ran Matlab/Octave Script after simulation.
    """
    _label = 'RunScript'
    _pattern = r'(?:[^\,"]+|\"[^\"]*\")'
    _title_sep = '='
    _attr_sep = ','

    RunScript: int


@dataclass
class showFrame(AbstractComponent):
    """ showFrame Property

        Parameters:
        _label (str, optional): Schematic label. Defaults to 'RunScript'.
        _pattern (str, optional): Schematic regex. Defaults to '(?:[^\\,"]+|\"[^\"]*\")'.
        _title_sep (str, optional): Character that separates title from attributes. Defaults to '='.
        _attr_sep (str, optional): Character that separates attributes. Defaults to ','.

        showFrame (int): Show Design Info Frame.
    """
    _label = 'showFrame'
    _pattern = r'(?:[^\,"]+|\"[^\"]*\")'
    _title_sep = '='
    _attr_sep = ','

    showFrame: int


@dataclass
class FrameText0(AbstractComponent):
    """ FrameText0 Property

        Parameters:
        _label (str, optional): Schematic label. Defaults to 'FrameText0'.
        _pattern (str, optional): Schematic regex. Defaults to '(?:[^\\,"]+|\"[^\"]*\")'.
        _title_sep (str, optional): Character that separates title from attributes. Defaults to '='.
        _attr_sep (str, optional): Character that separates attributes. Defaults to ','.

        Name (str): Show Design Info Title.
    """
    _label = 'FrameText0'
    _pattern = r'(?:[^\,"]+|\"[^\"]*\")'
    _title_sep = '='
    _attr_sep = ','

    Name: str


@dataclass
class FrameText1(AbstractComponent):
    """ FrameText1 Property

        Parameters:
        _label (str, optional): Schematic label. Defaults to 'FrameText1'.
        _pattern (str, optional): Schematic regex. Defaults to '(?:[^\\,"]+|\"[^\"]*\")'.
        _title_sep (str, optional): Character that separates title from attributes. Defaults to '='.
        _attr_sep (str, optional): Character that separates attributes. Defaults to ','.

        Name (str): Show Design Info Author.
    """
    _label = 'FrameText1'
    _pattern = r'(?:[^\,"]+|\"[^\"]*\")'
    _title_sep = '='
    _attr_sep = ','

    Name: str


@dataclass
class FrameText2(AbstractComponent):
    """ FrameText2 Property

        Parameters:
        _label (str, optional): Schematic label. Defaults to 'FrameText2'.
        _pattern (str, optional): Schematic regex. Defaults to '(?:[^\\,"]+|\"[^\"]*\")'.
        _title_sep (str, optional): Character that separates title from attributes. Defaults to '='.
        _attr_sep (str, optional): Character that separates attributes. Defaults to ','.

        Name (str): Show Design Info Date.
    """
    _label = 'FrameText2'
    _pattern = r'(?:[^\,"]+|\"[^\"]*\")'
    _title_sep = '='
    _attr_sep = ','

    Name: str


@dataclass
class FrameText3(AbstractComponent):
    """ FrameText3 Property

        Parameters:
        _label (str, optional): Schematic label. Defaults to 'FrameText3'.
        _pattern (str, optional): Schematic regex. Defaults to '(?:[^\\,"]+|\"[^\"]*\")'.
        _title_sep (str, optional): Character that separates title from attributes. Defaults to '='.
        _attr_sep (str, optional): Character that separates attributes. Defaults to ','.

        Name (str): Show Design Info Revision.
    """
    _label = 'FrameText3'
    _pattern = r'(?:[^\,"]+|\"[^\"]*\")'
    _title_sep = '='
    _attr_sep = ','

    Name: str
