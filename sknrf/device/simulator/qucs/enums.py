from enum import Enum


class ENABLE(Enum):
    ENABLE = 0
    DISABLE = 1
    SHORTED = 2


class MIRROR(Enum):
    FALSE = 0
    TRUE = 1


class ROTATION(Enum):
    NORTH = 0
    EAST = 1
    SOUTH = 2
    WEST = 3
