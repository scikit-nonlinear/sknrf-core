from dataclasses import dataclass

from sknrf.device.simulator.qucs.components.base import AbstractComponent


@dataclass
class Wire(AbstractComponent):
    """ Wire Component

        Parameters:
        _label (str, optional): Schematic label. Defaults to 'Wire'.

        x0 (int): Start X-Coordinate.
        y0 (int): Start Y-Coordinate.
        x1 (int): Stop X-Coordinate.
        y1 (int): Stop Y-Coordinate.
        Name (str): Port RefID.
        tx (int): X-Coordinate of Text-Box.
        ty (int): Y-Coordinate of Text-Box.
        u0 (int): Unknown Parameter 0.
        U0 (str): Initial Voltage.
    """
    _label = ''
    _pattern = r'(?:[^\s"]+|\"[^\"]*\")'
    _title_sep = ''
    _attr_sep = ' '

    x0: int
    y0: int
    x1: int
    y1: int
    name: str
    tx: int
    ty: int
    u0: int
    U0: str
