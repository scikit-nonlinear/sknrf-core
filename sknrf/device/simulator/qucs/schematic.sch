<Properties>
  <View=-8228,-2195,12740,2027,2.41,19459,5463>
  <Grid=10,10,1>
  <DataSet=schematic.dat>
  <DataDisplay=schematic.dpl>
  <OpenDisplay=0>
  <Script=schematic.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
  <.PortSym 40 20 1 0 P1P>
  <.PortSym 60 50 2 0 P1N>
</Symbol>
<Components>
  <Port P1P 1 100 -160 4 -46 0 2 "1" 1 "analog" 0 "v" 0 "" 0>
  <IProbe I_1H 1 100 -130 16 -26 0 1>
  <VProbe V_1H 1 130 -80 -15 -48 1 0>
  <BiasT X3 5 100 0 34 -26 0 1 "1e-6" 0 "1e-6" 0>
  <Vdc V_10 1 150 0 -26 18 1 2 "vm_10" 1>
  <GND * 1 170 -100 0 0 0 1>
  <Vac V11 1 130 100 -26 18 1 2 "vm_11" 1 "1e9" 1 "0" 1 "0" 0 "0" 0 "0" 0>
  <Vac V12 1 130 200 -26 18 0 0 "vm_12" 1 "2e9" 1 "90" 1 "0" 0 "0" 0 "0" 0>
  <Vac V13 1 130 300 -26 18 1 2 "vm_13" 1 "3e9" 1 "180" 1 "0" 0 "0" 0 "0" 0>
  <SPfile X5 1 130 400 -26 53 1 2 "G_1H.s1p" 1 "rectangular" 0 "linear" 0 "open" 0 "1" 0>
  <GND * 1 160 -260 0 0 0 1>
  <Eqn Eqn1 1 130 500 -31 18 0 0 "f_11=1e9" 1 "f_12=2e9" 1 "f_13=3e9" 1 "vm_10=1" 1 "vm_11=20.0" 1 "vm_12=20.0" 1 "vm_13=20.0" 1 "vp_10=0.0" 1 "vp_11=0.0" 1 "vp_12=90.0" 1 "vp_13=180" 1 "no" 0>
  <Port P1N 1 130 430 15 8 0 2 "2" 1 "analog" 0 "v" 0 "" 0>
  <.HB HB1 1 530 -150 0 70 0 0 "1e9" 0 "4" 1 "1e-12" 0 "1e-6" 0 "0.001" 0 "150" 0>
  <.SP SP1 1 520 140 0 70 0 0 "lin" 1 "1e6" 1 "100e6" 1 "200" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <.SW SW1 1 510 310 0 70 0 0 "" 1 "lin" 1 "R1" 1 "5" 1 "50" 1 "20" 1>
  <.DC DC1 1 500 540 0 41 0 0 "26.85" 0 "0.001" 0 "1e-12" 0 "1e-6" 0 "no" 0 "150" 0 "no" 0 "none" 0 "CroutLU" 0>
  <.AC AC1 1 500 660 0 41 0 0 "lin" 1 "1" 1 "10e3" 1 "200" 1 "no" 0>
  <.NOISE NOISE1 1 490 810 0 70 0 0 "lin" 1 "1" 1 "10e3" 1 "100" 1 "v(node1)" 1 "V1" 1>
  <.Digi Digi1 1 470 1030 0 70 0 0 "TruthTable" 1 "10e-9" 0 "VHDL" 0>
</Components>
<Wires>
  <100 -100 100 -30 "" 0 0 0 "">
  <100 200 100 300 "" 0 0 0 "">
  <160 300 160 400 "" 0 0 0 "">
  <170 0 180 0 "" 0 0 0 "">
  <120 0 130 0 "" 0 0 0 "">
  <100 -100 120 -100 "" 0 0 0 "">
  <170 -100 170 0 "" 0 0 0 "">
  <140 -100 170 -100 "" 0 0 0 "">
  <160 100 160 200 "" 0 0 0 "">
  <100 30 100 100 "" 0 0 0 "">
  <100 -260 100 -160 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
