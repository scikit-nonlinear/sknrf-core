/** **********************************************************************

 This file is Generated by ADS "hpeesofmake pt2sys" command	           
 SystemC users are not allowed to make any change in this file.           
 Modifying this file may result in a non-functional Ptolemy-SystemC       
 Cosim. If you need to modify your design interface then modify your      
 .pl file instead.                                                        

 Copyright (c) 2007 Agilent Technologies Inc. All rights reserved.         

***************************************************************************/




#ifndef SystemCPtolemyInterface_PT_SC_FIR_H_INCLUDE 
#define SystemCPtolemyInterface_PT_SC_FIR_H_INCLUDE


#include <systemc.h>

/** Forward declaration of opaque structures used in SystemCPtolemyInterface_PT_SC_FIR */
extern "C" 
 { 
	 typedef struct SCADS_InterfaceList_s SCADS_InterfaceList;
	 typedef struct SCADS_Connection_s SCADS_Connection;
	 typedef struct SCADS_Interface_s  SCADS_Interface;
}

/**
@class SystemCPtolemyInterface_PT_SC_FIR Interface class 
 to implement SystemC-ADS Cosim. 
 SystemC user will use this class to access variables, and
 connecting FIFO ports to the rest of the SystemC design.
 */
class SystemCPtolemyInterface_PT_SC_FIR : public sc_module { 
public: 

/** 
  Adding Interface Output ports.
  Please note that for a SyetmC users these are the inputs to their design, 
 and these are also the inputs in Ptolemy schematic, but for Interface 
 these are the outputs to SystemC user's design.
*/
	 sc_fifo_out<int> input; /**< Connect this to corresponding "INPUT" in your SystemC design.*/
 
/** 
  Adding Interface Input ports.
  Please note that for a SyetmC users these are the outputs from their design, 
 and these are also the outputs in Ptolemy schematic, but for Interface 
 these are the inputs from SystemC user's design.
*/
	 sc_fifo_in<int> output; /**< Connect this to corresponding "OUTPUT" in your SystemC design.*/
 

/** 
  Adding Interface Parameters.
  SysetmC user must only use following accessor functions to access these parameter values , 
 1. getParamArraySize to find number of array elements for this parameter
 2. getIntegerParamValue to get the integer value 
 3. getdoubleParamValue to get the double value 
*/
	 SCADS_Interface * Taps; /**< Parameter Interface.*/

	 SC_HAS_PROCESS(SystemCPtolemyInterface_PT_SC_FIR);

	 SystemCPtolemyInterface_PT_SC_FIR(sc_module_name name, char * connectiontoken);
	 ~SystemCPtolemyInterface_PT_SC_FIR();


/**  The accessor function to get the number of array elements for a user 
 defined parameter. It returns 1 for a scalar user defined parameter.
 @param param User-defined Paremeter of opaque type SCADS_Interface. 
@return Number of array elements for param.
*/
	 int getParamArraySize(SCADS_Interface * param);


/**  The accessor function to get the integer value of a user 
 defined parameter. 
 @param param User-defined Paremeter of opaque type SCADS_Interface. 
@param index Index of the Parameter array, default index=0.
@return The integer value of paramter.
*/
	 int getIntegerParamValue(SCADS_Interface * param, int index=0);


/**  The accessor function to get the double value of a user 
 defined parameter. 
 @param param User-defined Paremeter of opaque type SCADS_Interface. 
@param index Index of the Parameter array, default index=0.
@return The double value of paramter.
*/
	 double getDoubleParamValue(SCADS_Interface * param, int index=0);
private:
	 void go();	
	 SCADS_Connection * connection;
	 SCADS_InterfaceList * interfacelist;

	 SCADS_Interface * pin_input;
	 SCADS_Interface * pin_output;
};
void ptolemy_sc_main(int argc, char * argv[], SystemCPtolemyInterface_PT_SC_FIR & p);

#endif
