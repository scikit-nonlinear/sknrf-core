import os
import site

__version__ = '1.1.4'

# Environment Variable Fallback Values
os.environ['SKNRF_DIR'] = os.getenv('SKNRF_DIR', site.getsitepackages()[0])
os.environ['VISA_LIB'] = os.getenv('VISA_LIB', '@py')

