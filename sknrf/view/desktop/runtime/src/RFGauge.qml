import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Extras 1.4
import QtQuick.Layouts 1.11
import QtQuick.Controls.Material 2.0

Rectangle{
    Material.foreground: "white"
    Material.background: "white"
    Material.accent: "white"
    enum Format {
        RE,
        RE_IM,
        LIN_DEG,
        LOG_DEG
    }
    width: 400
    height: 100
    border.color: "#ffffff"
    property alias name: title.text
    property alias value: gauge.value

    Label {
        id: title
        text: "Name"
        styleColor: "#ffffff"
        color: "#151515"
        font.bold: true
        anchors.verticalCenter: gauge.verticalCenter
        anchors.right: gauge.left
        anchors.verticalCenterOffset: -13
        anchors.rightMargin: 5
    }
    Gauge {
        id: gauge
        orientation: Qt.Horizontal
        value: -9
        minimumValue: -9
        maximumValue: 9
        tickmarkStepSize: 3
        minorTickmarkCount: 2
        font.pixelSize: 15
        anchors.centerIn: parent
        anchors.horizontalCenterOffset: -4

        style: GaugeStyle {
            valueBar: Rectangle {
                color: Qt.rgba((gauge.value-gauge.minimumValue)/(gauge.maximumValue-gauge.minimumValue), 0, 1 - (gauge.value-gauge.minimumValue)/(gauge.maximumValue-gauge.minimumValue), 1)
                implicitWidth: 28
            }

            foreground: Rectangle {
                    color: "#33333333"
                }

            tickmarkLabel: Text {
                    property int format: RFGauge.Format.LOG_DEG
                    id: tick_label
                    text: format === RFGauge.Format.LOG_DEG ?
                              styleData.value + "dB" :
                              (styleData.value < 0 ? "1/" : "") + Math.round(Math.pow(Math.pow(10, Math.abs(styleData.value)/20), 2))
                    color: styleData.value > 0 ? "#e34c22" : "#151515"
                    antialiasing: true
                }

            tickmark: Item {
                implicitWidth: 8
                implicitHeight: 4
                Rectangle {
                    x: control.tickmarkAlignment === Qt.AlignLeft
                       || control.tickmarkAlignment === Qt.AlignTop ? parent.implicitWidth : -28
                    width: 28
                    height: parent.height
                    color: "#151515"
                }
            }

            minorTickmark: Item {
                implicitWidth: 8
                implicitHeight: 1

                Rectangle {
                    x: control.tickmarkAlignment === Qt.AlignLeft
                       || control.tickmarkAlignment === Qt.AlignTop ? parent.implicitWidth : -28
                    width: 28
                    height: parent.height
                    color: "#151515"
                }
            }
        }
    }
}
