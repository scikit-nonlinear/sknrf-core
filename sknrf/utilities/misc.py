def doc_figures():
    """
    .. image:: /_images/PNG/orange/32/lfsource.png
    .. image:: /_images/PNG/orange/32/lfreceiver.png
    .. image:: /_images/PNG/orange/32/lfztuner.png
    .. image:: /_images/PNG/orange/32/rfsource.png
    .. image:: /_images/PNG/orange/32/rfreceiver.png
    .. image:: /_images/PNG/orange/32/rfztuner.png

    .. image:: /_images/PNG/load_device/load_device1.png
    .. image:: /_images/PNG/load_device/load_device2.png
    .. image:: /_images/PNG/load_device/load_device3.png
    .. image:: /_images/PNG/load_device/load_device4.png
    .. image:: /_images/PNG/load_device/load_device5.png

    .. image:: /_images/PNG/sequencer/sequencer0.png
    .. image:: /_images/PNG/sequencer/sequencer1.png
    .. image:: /_images/PNG/sequencer/sequencer2.png
    .. image:: /_images/PNG/sequencer/sequencer3.png
    .. image:: /_images/PNG/sequencer/sequencer4.png
    .. image:: /_images/PNG/sequencer/sequencer5.png
    .. image:: /_images/PNG/sequencer/sequencer6.png
    .. image:: /_images/PNG/sequencer/sequencer7.png
    .. image:: /_images/PNG/sequencer/sequencer8.png
    .. image:: /_images/PNG/sequencer/sequencer9.png
    .. image:: /_images/PNG/sequencer/sequencer10.png
    .. image:: /_images/PNG/sequencer/sequencer11.png
    .. image:: /_images/PNG/sequencer/sequencer12.png
    .. image:: /_images/PNG/sequencer/sequencer13.png
    .. image:: /_images/PNG/sequencer/sequencer14.png
    .. image:: /_images/PNG/sequencer/sequencer15.png
    .. image:: /_images/PNG/sequencer/sequencer16.png
    """