.. sknrf/widget

..  figure:: ../../_images/PNG/sknrf_logo.png
    :width: 500 pt
    :align: center

widget
======

.. raw:: html

    <ul>
        <li class="toctree-l1"><a class="reference internal" href="progressindicator/html/index.html">progressindicator</a></li>
        <li class="toctree-l1"><a class="reference internal" href="propertybrowser/html/index.html">propertybrowser</a></li>
        <li class="toctree-l1"><a class="reference internal" href="rangeslider/html/index.html">rangeslider</a></li>
    </ul>


