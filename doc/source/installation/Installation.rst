.. sknrf documentation introduction file

..  figure:: ../_images/PNG/sknrf_logo.png
    :width: 500 pt
    :align: center

Installation
============

Operating Systems
-----------------

.. raw:: html

    <div class="container">
      <div class="row">
        <div class="col-md-9">

        <div class="row" align="center">
            <div class="col-sm-3">
                <div class="panel-heading">
                    <h2 class="panel-title" style="font-size:20px">Windows</h2>
                </div>
                <div class="panel-body" style="font-size:18px">
                    <a href="./Windows.html"><img src="../_images/PNG/black/64/windows8.png" class="img-circle" alt="Windows" width="40%" height="40%"></a>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="panel-heading">
                    <h2 class="panel-title" style="font-size:20px">Mac OS</h2>
                </div>
                <div class="panel-body" style="font-size:18px">
                    <a href="./MacOS.html"><img src="../_images/PNG/black/64/apple.png" class="img-circle" alt="macOS" width="40%" height="40%"></a>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="panel-heading">
                    <h2 class="panel-title" style="font-size:20px">Linux</h2>
                </div>
                <div class="panel-body" style="font-size:18px">
                    <a href="./Linux.html"><img src="../_images/PNG/black/64/tux.png" class="img-circle" alt="Linux" width="40%" height="40%"></a>
                </div>
            </div>
        </div>
        </div>
      </div>
    </div>

..  toctree::
    :maxdepth: 1

    Windows (Not supported 2018) <Windows>
    macOS (Not supported 2020) <MacOS>
    Linux <Linux>
    Linux Debian<Linux_Deb>
    Linux Debian Embedded<Linux_Debian_Embedded>
