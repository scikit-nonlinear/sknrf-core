
Automation
==========
Three incremental solutions are available for measurement automation:

1. Sequencer.
2. Scripting.
3. Application.

Sequencer
---------
By default, the Sequencer allows you to perform basic analysis using parametric sweep plans or optimization plans.
External applications can also publish sequencer objects and methods.

Scripting
---------
Python Scripts or Jupyter Notebooks can be used to perform sandbox experiments on top of the Core API.

Application
-----------
Polished applications can be loaded within the GUI that perform tasks specific to:
    - Calibration/De-embedding.
    - Characterization.
    - Modeling.
    - Design.
