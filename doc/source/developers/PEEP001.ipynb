{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# PEEP 001: Memory Optimized Signals"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Background\n",
    "\n",
    "Real-life signals exist in a measurement-space defined by:\n",
    "\n",
    "* time, \n",
    "* frequency, \n",
    "* and various parametric sweeps. \n",
    "    \n",
    "Propagating where a signal lies in the measurement space is useful for:\n",
    "\n",
    "* Plotting against the correct independent axis.\n",
    "* Plotting multi-dimensional data on a 2D axis using a legend.\n",
    "* Transforming data between domains (Time, Frequency, Time Envelope, Frequency Spectrum, Power)\n",
    "\n",
    "While, tracking independent data offers context and convenience, it increase computational costs of time-critical processes. Therefore, there must always be a way to opt-out of signal tracking, thus resorting to a numpy ndarray datatype."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## NumPy Multi-Dimensional Arrays\n",
    "\n",
    "NumPy arrays use C-style (Row-Major) memory order by default. A contiguous ND multi-dimensional array is organized a follows:\n",
    "* Axis order:                 axis0, axis1, ..., axisN-1\n",
    "* Memory Order:               largest memory step, ...., smallest memory step.\n",
    "* Multi-dimensional indexing: arr[index0, index1, ..., indexN-1].\n",
    "* Array of Array indexing:    arr[index0][index1, ..., indexN-1]."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## NumPy Indexing Behaviour\n",
    "\n",
    "NumPy supports a wide variety of multi-dimensional indexing behaviour that must be studied. Allthough different static functions exist for different indexing operations (eg. take, split, mask, etc), all non-static indexing of numpy arrays must be completed inside the __getitem__ magic method of the np.ndarray class. The different types of indexing operations supported by __getitem__ are distinguished as:\n",
    "* Basic Indexing and Slicing - Passes a scalar or tuple to __getitem__ and returns a view of the data.\n",
    "* Advanced Indexing and Slicing - Passes an ndarray of boolean or integers to __getitem__ and returns a copy of the data.\n",
    "\n",
    "The goal of this document is to apply memory optimized array indexing techniques to signal data, hence any advanced indexing behaviour will not be implemented in the Signal data type."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Basic Indexing and Slicing Inputs and Outputs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The types of basic indexing and slicing inputs are described and the output of such operations is demonstated using several examples. The types of inputs passed to the __getitem__ method can be any of the following:\n",
    "* int\n",
    "* slice\n",
    "* Ellipsis\n",
    "* newaxis\n",
    "* Any sequence (tuple, list) representing a multi-dimensional combination of the above data types."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Numeric Indexing\n",
    "<table style=\"width:100%\">\n",
    "  <tr>\n",
    "    <th>Inputs</th>\n",
    "    <th>Outputs</th> \n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>int</td>\n",
    "    <td>scalar or array of dim - 1</td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>sequence of ints (dim(seq) <= dim(array))</td> \n",
    "    <td>scalar or array of dim - dim(seq)</td> \n",
    "  </tr>\n",
    "</table>\n",
    "Positive (0 based) and/or negative (-1 based) indexing along multiple axis' will result in a scalar when all dimensions have been indexes, otherwise the remaining unindexed axis' will be used to construct a sub-array. **NOTE: The dimensions of the sub-array is reduced by the number of indicies.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "((), 1.0)"
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "arr = np.ones((1, 2, 3, 4))\n",
    "\n",
    "# Scalar output\n",
    "sub_arr = arr[0, 1, 2, 3]\n",
    "sub_arr.shape, sub_arr"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(2, 3, 4)"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Axis 0 index\n",
    "sub_arr = arr[0]\n",
    "sub_arr.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Slice Indexing\n",
    "<table style=\"width:100%\">\n",
    "  <tr>\n",
    "    <th>Inputs</th>\n",
    "    <th>Outputs</th> \n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>slice</td>\n",
    "    <td>array</td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>sequence of slices (dim(seq) <= dim(array))</td> \n",
    "    <td>array of dim(array)</td> \n",
    "  </tr>\n",
    "</table>\n",
    "\n",
    "Positive (0-based) and/or negative (-1 based) slicing along multiple axis' will result in a sub-array of the same number of dimensions. **NOTE: The dimensions of the sub-array is the same as the input array.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "((1, 1, 1, 1), array([[[[ 1.]]]]))"
      ]
     },
     "execution_count": 20,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "arr = np.ones((1, 2, 3, 4))\n",
    "\n",
    "# Single Value Slice (Preserves the dimensions)\n",
    "sub_arr = arr[0:1, 1:2, 2:3, 3:4]\n",
    "sub_arr.shape, sub_arr"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(1, 2, 3, 4)"
      ]
     },
     "execution_count": 21,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Axis 0 slice\n",
    "sub_arr = arr[0:1]\n",
    "sub_arr.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Ellipsis Indexing\n",
    "<table style=\"width:100%\">\n",
    "  <tr>\n",
    "    <th>Inputs</th>\n",
    "    <th>Outputs</th> \n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>Ellipsis</td>\n",
    "    <td>All non indexed arrays are replaced with : slice</td> \n",
    "  </tr>\n",
    "</table>\n",
    "\n",
    "High-Dimensional arrays can be efficiently handled using Ellipsis indexing to select all values along multiple dimesions. **NOTE: Only one Ellipsis is allowed in an indexing operation.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### NewAxis Indexing\n",
    "<table style=\"width:100%\">\n",
    "  <tr>\n",
    "    <th>Inputs</th>\n",
    "    <th>Outputs</th> \n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td>newaxis</td>\n",
    "    <td>A new dimension is added in the indexed dimension</td> \n",
    "  </tr>\n",
    "</table>\n",
    "\n",
    "Adds a new dimension to the resulting sub-array along the dimension where the newaxis dimension was placed."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Combining Indexation Types\n",
    "Any combination of integer, slice, Ellipsis, or newaxis indexation can be combined into a tuple that is passed to __getitem__. Other non-ndarray sequence types are also supported. An ndarray input will trigger Advancded indexing operations which are not backwards compatible with the basic indexation operations described above."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Proposed Solution\n",
    "\n",
    "An N-D signal will sub-class ndarray so that it also contains named independent variable information inside signal variable. This will ensure that indexation of the dependent data is reflected in the independnent data. \n",
    "\n",
    "### Memory Savings\n",
    "1. In a single measurement sweep, all derived dependent variables will contain a reference to a single memory copy of the independent sweep information. (Single Independent Varible memory for Multiple Dependent Variables)\n",
    "2. Uncoupled Measurement sweeps contains a 1-D slice of information compared to the N-D representation of the dependent variable. (1-D Independent Variable memory for N-D Dependent Varaibles).\n",
    "3. Coupled Independent sweeps still contain a small fraction of the memory allocated multi-dimensional dependent variables. \n",
    "\n",
    "### Considerations\n",
    "0. Advanced indexing operations should result in custom exception or NotImplemented or AdvancedIndexing,\n",
    "1. Sweeps should be **prepended** to signal array from right-to-left (fastest sweep-to-slowest sweep). This is opposite the axis increase from left-to-right of NumPy arrays.\n",
    "2. Integer (i) indexing should be replaced by slice(i, i+1, 1) indexing to maintain constant dimensions in signal.\n",
    "    * A squeeze method would create a controlled way of elminating singular axis'.\n",
    "3. NewAxis indexing should not be allowed because the number of sweeps (dims) should remain constant.\n",
    "4. Indexing should be from left-to-right to avoid confusion with NumPy arrays.\n",
    "    * Ellipsis operation can be used to perform calculations accross frequency/time (signal[..., freq, time]).\n",
    "5. Indepenent sweeps should be able to broadcast with depenent variables at all times. Can be specified in two formats:\n",
    "    - large shape: (sweep_ind, n, ..., m,...) where \"...\" represenents singular dimensions.\n",
    "    - small shape: sweep_ind, n, m, where \"n\", and \"m\" represent coupled sweep dimensions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Signal Class"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "indep_lock = (\"freq\", \"time\", \"row\", \"col\")\n",
    "indep_map = OrderedDict()\n",
    "init(arr, indep_map=OrderedDict, indep_lock=())\n",
    "    super(arr)\n",
    "    \\# Check Broadcasting\n",
    "    for ind, (k, v) in reversed(enumerate(indep_map)):\n",
    "        assert(v.shape[ind] == 1 or arr.shape[ind] == v.shape[ind])\n",
    "\n",
    "getitem(self, keys):\n",
    "    \\#Convert from scalar to tuple\n",
    "    if isinstance(keys, (int, type(Ellipsis), slice, newaxis))\n",
    "        keys = (keys)\n",
    "    elif isinstance(keys, (list, tuple))\n",
    "        keys = tuple(keys)\n",
    "    else:\n",
    "        raise AdvanceIndexingError()\n",
    "    new_keys = ()\n",
    "    for k, in keys:\n",
    "        if isinstance(k, int)\n",
    "            new_key += (slice(k, k+1, 1)) \\#Convert int indexing to slice\n",
    "        elif k is Ellipsis:\n",
    "            new_key += (slice(None))*(self.ndim-len(keys))\n",
    "        elif isinstance(k, newaxis):\n",
    "            raise NewAxisIndexingError()\n",
    "    arr = super().getitem(self, new_keys)\n",
    "    indep_map = self.indep_map.copy()\n",
    "    for k, v in self.indep_map:\n",
    "        indep_keys = (new_keys[ind] if x > 1 else slice(None) for ind, x in enumerate(indep_shape))\n",
    "        v  = v[indep_keys]\n",
    "    return Signal(arr, indep_map=indep_map)\n",
    "    \n",
    "    \n",
    "\n",
    "    \n",
    "    \n",
    "    [ new_keys[ind] if x > 1 else slice(None) for ind, x in enumerate(indep_shape)]\n",
    "    indep_keys[indep_shape > 1] = new_keys[indep_shape > 1]\n",
    "    \n",
    "    \n",
    "    \n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## IndepDict Class"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "add (self, key, value, coupled=())\n",
    "    /* Convert small to large shape\n",
    "    shape_ = (v.shape[ind] if k in coupled else 1 for ind, (k, v) in reversed(enumerate(self.items())))\n",
    "    shape = (-1) + shape_\n",
    "    self.prepend(key, value.reshape(shape_))\n",
    "    \n",
    "setitem (self, keys, value)\n",
    "    if key not in self:\n",
    "        raise KeyError(\"indep_sweep must be added first\")\n",
    "    super().setitem(keys, value)\n",
    "\n",
    "        "
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.4.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
