macro(export_package)
    # export_package(target version)
    # Export Package that is imported using find_package()
    install (EXPORT ${ARGV0}Targets
            FILE ${ARGV0}Targets.cmake
            NAMESPACE ${ARGV0}::
            DESTINATION lib/cmake/${ARGV0})
    add_library(${ARGV0}::${ARGV0} ALIAS ${ARGV0})

    include(CMakePackageConfigHelpers)
    ## generate the config file that is includes the exports
    configure_package_config_file(${CMAKE_CURRENT_SOURCE_DIR}/Config.cmake.in
            "${CMAKE_CURRENT_BINARY_DIR}/${ARGV0}Config.cmake"
            INSTALL_DESTINATION "lib/cmake/example"
            NO_SET_AND_CHECK_MACRO
            NO_CHECK_REQUIRED_COMPONENTS_MACRO
            )
    ## generate the version file for the config file
    write_basic_package_version_file(
            "${CMAKE_CURRENT_BINARY_DIR}/${ARGV0}ConfigVersion.cmake"
            VERSION ${ARGV1}
            COMPATIBILITY SameMajorVersion
    )

    ## install the configuration file
    install(FILES
            ${CMAKE_CURRENT_BINARY_DIR}/${ARGV0}Config.cmake
            ${CMAKE_CURRENT_BINARY_DIR}/${ARGV0}ConfigVersion.cmake
            DESTINATION lib/cmake/${ARGV0}
            )
endmacro()
