cmake_minimum_required(VERSION 3.26.0)
#include(cmake/toolchain.cmake)
project(sknrf)

# Include
include("${PROJECT_SOURCE_DIR}/cmake/root.cmake")

# Top-Level Macros
## Find package only when it is not a subproject
macro(find_package)
    if(NOT ${ARGV0} IN_LIST subprojects)
        _find_package(${ARGV})
    endif()
endmacro()

# universe
set(subprojects
	qprogressindicator qprogressindicatorplugin qprogressindicatorpy
	qrangeslider qrangesliderplugin qrangesliderpy
	qtpropertybrowser qtpropertybrowserplugin qtpropertybrowserpy qtpropertybrowsertest
)
set(DOC_PATH "doc/html/sknrf/widget/progressindicator")
add_subdirectory(${CMAKE_SOURCE_DIR}/sknrf/view/widget/qprogressindicator)
set(DOC_PATH "doc/html/sknrf/widget/rangeslider")
add_subdirectory(${CMAKE_SOURCE_DIR}/sknrf/view/widget/qrangeslider)
set(DOC_PATH "doc/html/sknrf/widget/propertybrowser")
add_subdirectory(${CMAKE_SOURCE_DIR}/sknrf/view/widget/qtpropertybrowser)
