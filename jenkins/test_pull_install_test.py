import unittest
import os

__author__ = 'dtbespal'


class TestInstall(unittest.TestCase):

    def test_cmake(self):
        pass

    def test_ninja(self):
        pass

    def test_skbuild(self):
        import skbuild

    def test_lark(self):
        import lark

    def test_lxml(self):
        import lxml

    def test_sphinx(self):
        import sphinx

    def test_virtualenv(self):
        pass

    def test_opengl(self):
        pass

    def test_opencl(self):
        import pyopencl

    def test_cython(self):
        pass

    def test_numpy(self):
        import numpy

    def test_numpy_doc(self):
        import numpydoc

    def test_scipy(self):
        import scipy

    def test_matplotlib(self):
        import matplotlib

    def test_qt(self):
        pass

    def test_pyside2(self):
        import PySide2

    def test_pybind11(self):
        import pybind11

    def test_yaml(self):
        import yaml

    def test_toml(self):
        import toml

    def test_pyvisa(self):
        import pyvisa

    def test_magma(self):
        pass

    def test_pytorch(self):
        import torch

    def test_vitis_kernels(self):
        pass

    def test_pytables(self):
        import tables

    def test_skrf(self):
        import skrf

    def test_statsmodels(self):
        import statsmodels

    def test_paramiko(self):
        import paramiko

    def test_toposort(self):
        import toposort

    def test_nose_exclude(self):
        import nose_exclude

    def test_pytest(self):
        import pytest

    def test_sphinx_bootcamp_theme(self):
        import sphinx_bootstrap_theme

    def test_pylint(self):
        import pylint

    def test_radon(self):
        import radon

    def test_sknrf(self):
        import sknrf